<?php

namespace ReefExtra\Likert;

use \Reef\Components\FieldValue;

class LikertValue extends FieldValue {
	
	protected $a_values;
	
	private function getStatementNames() {
		return array_column($this->Field->getDeclaration()['statements']??[], 'name');
	}
	
	/**
	 * @inherit
	 */
	public function validate() : bool {
		$this->a_errors = [];
		
		$a_missingValues = array_diff($this->getStatementNames(), array_keys($this->a_values));
		
		if(count($a_missingValues) > 0) {
			$this->a_errors[] = 'Missing values for '.implode(', ', $a_missingValues).' ';
			return false;
		}
		
		$a_declaration = $this->Field->getDeclaration();
		$i_min = -1 * count($a_declaration['nonscales']);
		$i_max = count($a_declaration['scales']);
		foreach($this->a_values as $s_name => $i_value) {
			if($i_value < $i_min || $i_value > $i_max) {
				$this->a_errors[] = 'Invalid value '.$i_value;
				return false;
			}
		}
		
		return true;
	}
	
	private function getDefault() {
		$a_default = [];
		
		$i_default = 0;
		foreach($this->Field->getDeclaration()['scales']??[] as $a_scale) {
			if($a_scale['default']??false) {
				$i_default = (int)str_replace('option_', '', $a_scale['name']);
				break;
			}
		}
		
		if($i_default === 0) {
			foreach($this->Field->getDeclaration()['nonscales']??[] as $a_nonscale) {
				if($a_nonscale['default']??false) {
					$i_default = -1 * (int)str_replace('option_', '', $a_nonscale['name']);
					break;
				}
			}
		}
		
		foreach($this->Field->getDeclaration()['statements']??[] as $a_statement) {
			$a_default[$a_statement['name']] = $i_default;
		}
		return $a_default;
	}
	
	/**
	 * @inherit
	 */
	public function fromDefault() {
		$this->a_values = $this->getDefault();
		$this->a_errors = null;
	}
	
	/**
	 * @inherit
	 */
	public function isDefault() : bool {
		return ($this->a_values == $this->getDefault());
	}
	
	/**
	 * @inherit
	 */
	public function fromUserInput($a_input) {
		$this->fromStructured($a_input);
	}
	
	/**
	 * @inherit
	 */
	public function fromStructured($a_input) {
		$a_default = $this->getDefault();
		
		$this->a_values = [];
		
		foreach($this->Field->getDeclaration()['statements']??[] as $a_statement) {
			$this->a_values[$a_statement['name']] = (int)($a_input[$a_statement['name']] ?? $a_default[$a_statement['name']]);
		}
		
		$this->a_errors = null;
	}
	
	/**
	 * @inherit
	 */
	public function toFlat() : array {
		return array_map('intval', $this->a_values);
	}
	
	/**
	 * @inherit
	 */
	public function fromFlat(array $a_flat) {
		$this->a_values = array_map('intval', $a_flat);
		$this->a_errors = null;
	}
	
	/**
	 * @inherit
	 */
	public function toStructured() {
		return $this->a_values;
	}
	
	/**
	 * @inherit
	 */
	public function toOverviewColumns() : array {
		return array_map('intval', $this->a_values);
	}
	
	/**
	 * @inherit
	 */
	public function toTemplateVar() {
		return $this->a_values;
	}
}
