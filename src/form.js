Reef.addComponent((function() {
	
	'use strict';
	
	var Field = function(Reef, $field) {
		this.$field = $field;
		this.Reef = Reef;
	};
	
	Field.componentName = 'reef-extra:likert';
	
	Field.viewVars = function(declaration) {
		var i, j, l;
		
		for(i in declaration.statements) {
			for(l in declaration.statements[i].locale) {
				declaration.statements[i].title = declaration.statements[i].locale[l];
				break;
			}
			
			declaration.statements[i].scales = [];
			for(j in declaration.scales) {
				declaration.statements[i].scales.push({
					'value' : declaration.scales[j].name,
					'checked' : declaration.scales[j].default,
					'default' : declaration.scales[j].default
				});
			}
			
			declaration.statements[i].nonscales = [];
			for(j in declaration.nonscales) {
				declaration.statements[i].nonscales.push({
					'value' : declaration.nonscales[j].name,
					'checked' : declaration.nonscales[j].default,
					'default' : declaration.nonscales[j].default
				});
			}
		}
		
		for(i in declaration.scales) {
			for(l in declaration.scales[i].locale) {
				declaration.scales[i].title = declaration.scales[i].locale[l];
				break;
			}
		}
		
		for(i in declaration.nonscales) {
			for(l in declaration.nonscales[i].locale) {
				declaration.nonscales[i].title = declaration.nonscales[i].locale[l];
				break;
			}
		}
		
		return declaration;
	};
	
	Field.prototype.attach = function() {
		
	};
	
	Field.prototype.getValue = function() {
		var values = {};
		this.$field.find('tbody tr').each(function() {
			var $tr = $(this);
			values[$tr.attr('data-name')] = $tr.find('input:checked').val() || 0;
		});
		return values;
	};
	
	Field.prototype.setValue = function(values) {
		if(values == 'default') {
			this.$field.find('tbody tr input').each(function() {
				var $input = $(this);
				$input.prop('checked', !!$input.attr('data-default'));
			});
		}
		
		this.$field.find('tbody tr').each(function() {
			var $tr = $(this);
			var $inputs = $tr.find('input');
			$inputs.prop('checked', false);
			
			if(values == 'default') {
				$inputs.filter('[data-default]').prop('checked', true);
			}
			else {
				var name = $tr.attr('data-name');
				if(typeof values[name] !== 'undefined') {
					$inputs.filter('[value="'+values[name]+'"]').prop('checked', true);
				}
				else {
					$inputs.filter('[data-default]').prop('checked', true);
				}
			}
		});
		
		this.$field.trigger(EVTPRFX+'change');
	};
	
	Field.prototype.toDefault = function() {
		this.setValue('default');
	};
	
	Field.prototype.validate = function() {
		this.removeErrors();
		
		return true;
	};
	
	Field.prototype.setError = function(message_key) {
		this.$field.addClass(CSSPRFX+'invalid');
		
		if(this.Reef.config.layout_name == 'bootstrap4') {
			this.$field.find('input').addClass('is-invalid');
			this.$field.find('.invalid-feedback').hide().filter('.'+CSSPRFX+message_key).show();
		}
	};
	
	Field.prototype.removeErrors = function() {
		this.$field.removeClass(CSSPRFX+'invalid');
		
		if(this.Reef.config.layout_name == 'bootstrap4') {
			this.$field.find('input').removeClass('is-invalid');
			this.$field.find('.invalid-feedback').hide();
		}
	};
	
	Field.prototype.addError = function(message) {
		this.$field.addClass(CSSPRFX+'invalid');
		
		if(this.Reef.config.layout_name == 'bootstrap4') {
			this.$field.find('input').addClass('is-invalid');
			this.$field.find('input').last().parent().append($('<div class="invalid-feedback"></div>').text(message));
		}
	};
	
	return Field;
})());
