<?php

namespace ReefExtra\Likert;

use \Reef\Components\Component;

class LikertComponent extends Component {
	
	const COMPONENT_NAME = 'reef-extra:likert';
	const PARENT_NAME = null;
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function validateDeclaration(array $a_declaration, array &$a_errors = null) : bool {
		$b_valid = true;
		
		$i_defaults = 0;
		foreach($a_declaration['scales']??[] as $a_scale) {
			if(\Reef\interpretBool($a_scale['default'] ?? false)) {
				$i_defaults++;
			}
		}
		foreach($a_declaration['nonscales']??[] as $a_nonscale) {
			if(\Reef\interpretBool($a_nonscale['default'] ?? false)) {
				$i_defaults++;
			}
		}
		
		foreach(['scales', 'nonscales'] as $s_type) {
			$i=0;
			foreach($a_declaration[$s_type] as $j => $a_option) {
				if($j != $i || (int)str_replace('option_', '', $a_option['name']) != $i+1) {
					$a_errors['scales'] = "Invalid ".$s_type." name";
					$b_valid = false;
				}
				$i++;
			}
		}
		
		if($i_defaults > 1) {
			$a_errors['scales'] = "Please provide at most one default value";
			$b_valid = false;
		}
		
		return $b_valid;
	}
	
	/**
	 * @inherit
	 */
	public function getJS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'form.js',
				'view' => 'form',
			]
		];
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'form.css',
				'view' => 'form',
			]
		];
	}
	
	/**
	 * @inherit
	 */
	public function supportedStorages() : ?array {
		return [
			'pdo_mysql',
			'pdo_sqlite',
		];
	}
	
}
