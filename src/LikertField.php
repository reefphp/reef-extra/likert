<?php

namespace ReefExtra\Likert;

use \Reef\Components\Field;
use \Reef\Updater;

class LikertField extends Field {
	
	const MAX_SCALED_VALS = 15;  //  1 through 15 for scaled values
	const MAX_UNSCALED_VALS = 5; // -5 through -1 for unscaled values
	
	/**
	 * @inherit
	 */
	public function getFlatStructure() : array {
		$a_columns = [];
		
		foreach($this->a_declaration['statements']??[] as $a_statement) {
			// Double the ranges to ease migrations
			// This should not be a problem, as the maximum range will probably never
			// be larger than a few dozens, which fits perfectly in the smallest of ints
			$a_columns[$a_statement['name']] = [
				'type' => \Reef\Storage\Storage::TYPE_INTEGER,
				'min' => 2 * -1 * static::MAX_UNSCALED_VALS,
				'max' => 2 * static::MAX_SCALED_VALS,
			];
		}
		
		return $a_columns;
	}
	
	/**
	 * @inherit
	 */
	public function getOverviewColumns() : array {
		$Reef = $this->getComponent()->getReef();
		$a_langs = $Reef->getOption('locales');
		array_unshift($a_langs, $Reef->getOption('default_locale'));
		$a_langs = array_unique($a_langs);
		
		$a_columns = [];
		
		foreach($this->a_declaration['statements']??[] as $i => $a_statement) {
			
			$s_title = '';
			foreach($a_langs as $s_lang) {
				if(!empty($a_statement['locale'][$s_lang])) {
					$s_title = $a_statement['locale'][$s_lang];
					break;
				}
			}
			
			$a_columns[$a_statement['name']] = $s_title;
		}
		
		return $a_columns;
	}
	
	/**
	 * @inherit
	 */
	public function view_form($Value, $a_options = []) : array {
		$a_vars = parent::view_form($Value, $a_options);
		$a_vars['values'] = (array)$Value->toTemplateVar();
		
		$a_values = (array)$Value->toStructured();
		$a_statements = [];
		
		$Reef = $this->getComponent()->getReef();
		$a_langs = $Reef->getOption('locales');
		array_unshift($a_langs, $Reef->getOption('default_locale'));
		$a_langs = array_unique($a_langs);
		
		$a_vars['statements'] = $a_vars['statements'] ?? [];
		$a_vars['scales'] = $a_vars['scales'] ?? [];
		$a_vars['nonscales'] = $a_vars['nonscales'] ?? [];
		
		foreach($a_vars['scales'] as $i => &$a_scale) {
			$s_title = '';
			foreach($a_langs as $s_lang) {
				if(!empty($a_scale['locale'][$s_lang])) {
					$s_title = $a_scale['locale'][$s_lang];
					break;
				}
			}
			$a_scale['title'] = $s_title;
			$a_scale['value'] = (int)str_replace('option_', '', $a_scale['name']);
		}
		
		foreach($a_vars['nonscales'] as $i => &$a_nonscale) {
			$s_title = '';
			foreach($a_langs as $s_lang) {
				if(!empty($a_nonscale['locale'][$s_lang])) {
					$s_title = $a_nonscale['locale'][$s_lang];
					break;
				}
			}
			$a_nonscale['title'] = $s_title;
			$a_nonscale['value'] = -1 * (int)str_replace('option_', '', $a_nonscale['name']);
		}
		
		foreach($a_vars['statements'] as $i => $a_statement) {
			
			$s_title = '';
			foreach($a_langs as $s_lang) {
				if(!empty($a_statement['locale'][$s_lang])) {
					$s_title = $a_statement['locale'][$s_lang];
					break;
				}
			}
			
			$a_scales = $a_nonscales = [];
			foreach($a_vars['scales'] as &$a_scale) {
				$a_scales[] = [
					'value' => $a_scale['value'],
					'checked' => isset($a_values[$a_statement['name']]) ? $a_values[$a_statement['name']] == $a_scale['value'] : ($a_scale['default']??false),
					'default' => $a_scale['default']??false,
				];
			}
			foreach($a_vars['nonscales'] as &$a_nonscale) {
				$a_nonscales[] = [
					'value' => $a_nonscale['value'],
					'checked' => isset($a_values[$a_statement['name']]) ? $a_values[$a_statement['name']] == $a_nonscale['value'] : ($a_nonscale['default']??false),
					'default' => $a_nonscale['default']??false,
				];
			}
			
			$a_statements[$i] = [
				'name' => $a_statement['name'],
				'title' => $s_title,
				'scales' => $a_scales,
				'nonscales' => $a_nonscales,
			];
		}
		
		$a_vars['statements'] = $a_statements;
		$a_vars['spacer'] = (!empty($a_vars['scales']) && !empty($a_vars['nonscales']));
		
		return $a_vars;
	}
	
	/**
	 * @inherit
	 */
	public function view_submission($Value, $a_options = []) : array {
		return $this->view_form($Value, $a_options);
	}
	
	private function getUpdatePlan(Field $OldField, Field $NewField, string $s_property) {
		$a_oldOptions = array_column($OldField->a_declaration[$s_property] ?? [], 'name');
		$a_newOptions = array_column($NewField->a_declaration[$s_property] ?? [], 'name');
		
		$a_reNames = [];
		foreach($NewField->a_declaration[$s_property] ?? [] as $a_option) {
			if(isset($a_option['old_name'])) {
				$a_reNames[$a_option['old_name']] = $a_option['name'];
			}
		}
		if($s_property == 'statements') {
			foreach($NewField->a_declaration[$s_property] ?? [] as $a_option) {
				if(in_array($a_option['name'], $a_oldOptions) && !isset($a_reNames[$a_option['name']]) && !in_array($a_option['name'], $a_reNames)) {
					$a_reNames[$a_option['name']] = $a_option['name'];
				}
			}
		}
		
		$a_update = $a_reNames;
		$a_create = array_diff($a_newOptions, $a_reNames);
		$a_delete = array_diff($a_oldOptions, array_keys($a_reNames));
		
		return [$a_create, $a_update, $a_delete];
	}
	
	/**
	 * @inherit
	 */
	public function updateDataLoss($OldField) {
		[$a_createStmts, $a_updateStmts, $a_deleteStmts] = $this->getUpdatePlan($OldField, $this, 'statements');
		if(count($a_deleteStmts) > 0) {
			return Updater::DATALOSS_DEFINITE;
		}
		
		// If the scales values changed, we reset all positive values to 0
		if(count($OldField->a_declaration['scales']) != count($this->a_declaration['scales'])) {
			return Updater::DATALOSS_DEFINITE;
		}
		
		[$a_createNScl, $a_updateNScl, $a_deleteNScl] = $this->getUpdatePlan($OldField, $this, 'nonscales');
		if(count($a_deleteNScl) > 0) {
			return Updater::DATALOSS_POTENTIAL;
		}
		
		return Updater::DATALOSS_NO;
	}
	
	/**
	 * @inherit
	 */
	public function needsSchemaUpdate($OldField) {
		[$a_createStmts, $a_updateStmts, $a_deleteStmts] = $this->getUpdatePlan($OldField, $this, 'statements');
		
		// We need a schema update if there are statement renames
		if(count(array_filter($a_updateStmts, function($v, $k) { return $v != $k; }, ARRAY_FILTER_USE_BOTH)) > 0) {
			return true;
		}
		
		// We need a schema update if the number of scales values changed
		if(count($OldField->a_declaration['scales']) != count($this->a_declaration['scales'])) {
			return true;
		}
		
		// We need a schema update if scaled values were renamed (excluding renaming to the same name)
		[$a_createScl, $a_updateScl, $a_deleteScl] = $this->getUpdatePlan($OldField, $this, 'scales');
		$a_updateScl = array_filter($a_updateScl, function($v, $k) { return $v != $k; }, ARRAY_FILTER_USE_BOTH);
		if(count($a_updateScl) > 0) {
			return true;
		}
		
		// We need a schema update if nonscaled values were added, deleted or renamed (excluding renaming to the same name)
		[$a_createNScl, $a_updateNScl, $a_deleteNScl] = $this->getUpdatePlan($OldField, $this, 'nonscales');
		$a_updateNScl = array_filter($a_updateNScl, function($v, $k) { return $v != $k; }, ARRAY_FILTER_USE_BOTH);
		if(count($a_createNScl) > 0 || count($a_updateNScl) > 0 || count($a_deleteNScl) > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @inherit
	 */
	public function beforeSchemaUpdate($a_data) {
		[$a_createStmts, $a_updateStmts, $a_deleteStmts] = $this->getUpdatePlan($this, $a_data['new_field'], 'statements');
		[$a_createScl, $a_updateScl, $a_deleteScl] = $this->getUpdatePlan($this, $a_data['new_field'], 'scales');
		[$a_createNScl, $a_updateNScl, $a_deleteNScl] = $this->getUpdatePlan($this, $a_data['new_field'], 'nonscales');
		
		// Rename statements
		foreach($a_updateStmts as $s_oldName => $s_newName) {
			if($s_oldName == $s_newName) {
				continue;
			}
			
			switch($a_data['storageFactoryName']) {
				case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
				case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
					$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$a_data['new_columns'][$s_newName].' = '.$a_data['old_columns'][$s_oldName].' ');
				break;
			}
		}
		
		if(count($this->a_declaration['scales']) != count($a_data['new_field']->a_declaration['scales'])) {
			// Values were created and/or deleted. Hence, the distribution is not valid any more, so we should reset all values
			foreach($this->a_declaration['statements']??[] as $a_statement) {
				$s_statementName = $a_statement['name'];
				switch($a_data['storageFactoryName']) {
					case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
					case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
						$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$a_data['old_columns'][$s_statementName].' = 0 WHERE '.$a_data['old_columns'][$s_statementName].' > 0 ');
					break;
				}
			}
		}
		else {
			foreach($this->a_declaration['statements']??[] as $a_statement) {
				$s_column = $a_data['old_columns'][$a_statement['name']];
				$b_updated = false;
				
				// Update renamed scaled values.
				// Move to temporary interval [MAX_SCALED_VALS+1, 2*MAX_SCALED_VALS] to prevent collisions
				foreach($a_updateScl as $s_oldSclName => $s_newSclName) {
					if($s_oldSclName == $s_newSclName) {
						continue;
					}
					$b_updated = true;
					
					$i_old = (int)str_replace('option_', '', $s_oldSclName);
					$i_new = (int)str_replace('option_', '', $s_newSclName) + static::MAX_SCALED_VALS;
					
					switch($a_data['storageFactoryName']) {
						case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
						case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
							$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$s_column.' = '.$i_new.' WHERE '.$s_column.' = '.$i_old.' ');
						break;
					}
				}
				
				if($b_updated) {
					// Finish renaming update
					switch($a_data['storageFactoryName']) {
						case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
						case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
							$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$s_column.' = '.$s_column.' - '.((int)static::MAX_SCALED_VALS).' WHERE '.$s_column.' > '.((int)static::MAX_SCALED_VALS).' ');
						break;
					}
				}
			}
		}
		
		if(count($a_createNScl) > 0 || count($a_updateNScl) > 0 || count($a_deleteNScl) > 0) {
			foreach($this->a_declaration['statements']??[] as $a_statement) {
				$s_column = $a_data['old_columns'][$a_statement['name']];
				$b_updated = false;
				
				// Update renamed non-scaled values.
				// Move to temporary interval [-2*MAX_UNSCALED_VALS, -MAX_UNSCALED_VALS-1] to prevent collisions
				foreach($a_updateNScl as $s_oldNSclName => $s_newNSclName) {
					if($s_oldNSclName == $s_newNSclName) {
						continue;
					}
					$b_updated = true;
					
					$i_old = -1 * (int)str_replace('option_', '', $s_oldNSclName);
					$i_new = -1 * (int)str_replace('option_', '', $s_newNSclName) - static::MAX_UNSCALED_VALS;
					
					switch($a_data['storageFactoryName']) {
						case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
						case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
							$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$s_column.' = '.$i_new.' WHERE '.$s_column.' = '.$i_old.' ');
						break;
					}
				}
				
				// Remove deleted non-scaled values
				foreach($a_deleteNScl as $s_oldNSclName) {
					$i_old = -1 * (int)str_replace('option_', '', $s_oldNSclName);
					
					switch($a_data['storageFactoryName']) {
						case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
						case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
							$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$s_column.' = 0 WHERE '.$s_column.' = '.$i_old.' ');
						break;
					}
				}
				
				if($b_updated) {
					// Finish renaming update
					switch($a_data['storageFactoryName']) {
						case \Reef\Storage\PDO_SQLite_StorageFactory::getName():
						case \Reef\Storage\PDO_MySQL_StorageFactory::getName():
							$a_data['content_updater']('UPDATE '.$a_data['table'].' SET '.$s_column.' = '.$s_column.' + '.((int)static::MAX_UNSCALED_VALS).' WHERE '.$s_column.' < -'.((int)static::MAX_UNSCALED_VALS).' ');
						break;
					}
				}
			}
		}
		
	}
}
