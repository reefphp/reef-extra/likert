<?php

namespace ReefExtra\LikertTests;

use \ReefTests\integration\Components\ComponentTestCase;

final class LikertComponentTest extends ComponentTestCase {
	
	protected function createComponent() {
		return new \ReefExtra\Likert\LikertComponent;
	}
}
