<?php

namespace ReefExtra\LikertTests;

use \ReefTests\integration\Components\UpdateTestCase;

final class LikertUpdateTest extends UpdateTestCase {
	
	protected function createComponent() {
		return new \ReefExtra\Likert\LikertComponent;
	}
}
