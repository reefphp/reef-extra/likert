<?php

namespace ReefExtra\LikertTests;

use \ReefTests\integration\Components\FieldValueTestCase;

final class LikertValueTest extends FieldValueTestCase {
	
	protected function createComponent() {
		return new \ReefExtra\Likert\LikertComponent;
	}
}
