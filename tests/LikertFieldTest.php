<?php

namespace ReefExtra\LikertTests;

use \ReefTests\integration\Components\FieldTestCase;

final class LikertFieldTest extends FieldTestCase {
	
	protected function createComponent() {
		return new \ReefExtra\Likert\LikertComponent;
	}
}
